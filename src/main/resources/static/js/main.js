$(document).ready(function () {
    var option = '';
    for (var i = 1918; i <= 2010; i++) {
        option += '<option ' + (i === 1990 ? 'selected' : '') + ' value="' + i + '">' + i + '</option>';
    }
    $('#birthYear').append(option);

    var form = $("#registrationForm");
    var errorMessage = $("#errorAlert");
    var successMessage = $("#successAlert");

    form.on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: '/api/registrations',
            type: 'POST',
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            data: JSON.stringify({
                email: $('#email').val(),
                firstName: $('#firstName').val(),
                lastName: $('#lastName').val(),
                licenceNumber: $('#licenceNumber').val(),
                birthYear: $('#birthYear').val(),
                category: $('[name=category]:checked').val(),
                weapon: $('[name=weapon]:checked').val(),
                ownWeapon: $('[name=ownWeapon]:checked').val() === 'yes',
                preferredTime: $('[name=preferredTime]:checked').val()
            }),
            success: function (response, status) {
                form[0].reset();
                form.attr('hidden', 'hidden');
                errorMessage.attr('hidden', 'hidden');
                successMessage.removeAttr('hidden');

            },
            error: function (response, status) {
                form.removeAttr('hidden');
                errorMessage.removeAttr('hidden');
                successMessage.attr('hidden', 'hidden');
                console.log('error');
            }
        });
    });
});