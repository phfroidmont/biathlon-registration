package phf.biathlon

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.content.*
import io.ktor.features.CallLogging
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.features.AutoHeadResponse
import io.ktor.gson.gson
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.route
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import io.ktor.server.netty.NettyApplicationEngine
import org.apache.commons.mail.DefaultAuthenticator
import org.apache.commons.mail.SimpleEmail
import org.slf4j.event.Level
import java.io.File
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import javax.mail.internet.InternetAddress

data class Registration(
        val email: String,
        val lastName: String,
        val firstName: String,
        val licenceNumber: String,
        val birthYear: Int,
        val category: String,
        val weapon: String,
        val ownWeapon: Boolean,
        val preferredTime: String) {
    fun isValid() = email.length <= 100 &&
            lastName.length <= 100 &&
            firstName.length <= 100 &&
            birthYear >= 1918 && birthYear <= 2010 &&
            (category == "walk" || category == "run") &&
            (weapon == "pistol" || weapon == "rifle") &&
            (preferredTime == "none" || preferredTime == "morning" || preferredTime == "afternoon")
}

data class ErrorMessage(val message: String)

data class Config(val smtpHost: String,
                  val smtpPort: String,
                  val smtpEmail: String,
                  val smtpPassword: String)

class RegistrationsApp {
    private val registrationsWriter = RegistrationsWriter()
    private val server: NettyApplicationEngine = embeddedServer(Netty, port = 8080) {
        install(DefaultHeaders)
        install(CallLogging)
        install(AutoHeadResponse)
        install(ContentNegotiation) {
            gson {}
        }

        routing {
            static("/") {
                resources("static")
                defaultResource("static/index.html")
            }
            route("/api") {
                post("/registrations") {
                    val registration = call.receive<Registration>()
                    if (registration.isValid()) {
                        registrationsWriter.write(registration)
                        call.respond(registration)
                    } else {
                        call.respond(HttpStatusCode.BadRequest, ErrorMessage("Formulaire non valide"))
                    }
                }
            }
        }
    }


    init {
        server.start(wait = true)
    }

}

class RegistrationsWriter {
    private val gson: Gson
    private val registrationQueue = ConcurrentLinkedQueue<Registration>()
    private val registrations: MutableList<Registration>
    private val registrationsFile: File
    private var shutdown = false
    private val config: Config
    private val writerThread: Thread = Thread {
        while (!shutdown) {
            Thread.sleep(60000)
            writeNextRegistration()
        }
    }

    init {
        File("storage/").mkdirs()
        registrationsFile = File("storage/registrations.json")
        registrationsFile.createNewFile()
        gson = GsonBuilder().setPrettyPrinting().create()
        config = gson.fromJson(File("storage/config.json").readText(), Config::class.java)
        val registrationsString = registrationsFile.readText()
        registrations = if (registrationsString.isEmpty())
            ArrayList()
        else
            gson.fromJson<List<Registration>>(registrationsString, object : TypeToken<List<Registration>>() {}.type).toMutableList()

        writerThread.start()
    }

    private fun writeNextRegistration() {
        if (registrationQueue.size > 0) {
            val newRegistration = registrationQueue.poll()
            println(newRegistration)
            registrations.add(newRegistration)
            registrationsFile.writeText(gson.toJson(registrations))
            sendMail(newRegistration)
        }
    }

    private fun sendMail(registration: Registration) {
        try {
            SimpleEmail().apply {
                hostName = config.smtpHost
                setSmtpPort(465)
                setAuthenticator(DefaultAuthenticator(config.smtpEmail, config.smtpPassword))
                isSSLOnConnect = true
                setFrom(config.smtpEmail)
                setBcc(Collections.singleton(InternetAddress("info-biathlon@froidmont.org")))
                subject = "Inscription biathlon Bertrix 2019"
                setMsg(generateMailBody(registration))
                addTo(registration.email)
            }.send()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun write(registration: Registration) {
        registrationQueue.add(registration)
    }
}

fun generateMailBody(registration: Registration) =
        """
    Bonjour ${registration.firstName} ${registration.lastName},

    Nous vous confirmons par la présente que vous êtes bien inscrit(e) au biathlon de Bertrix.
    Vous avez fourni les informations suivantes:
    Numéro de licence: ${if (registration.licenceNumber.isBlank()) "aucun" else registration.licenceNumber}
    Année de naissance: ${registration.birthYear}
    Catégorie: ${if (registration.category == "walk") "Marche" else "Course"}
    Type d'arme: ${if (registration.weapon == "pistol") "Pistolet" else "Carabine"}
    Arme personnelle: ${if (registration.ownWeapon) "Oui" else "Non"}
    Préférence horaire: ${if (registration.preferredTime == "none") "Aucune" else if (registration.preferredTime == "morning") "Matin" else "Après-midi"}

    Salutations sportives,
    Les organisateurs du biathlon.

    PS: En cas d'erreurs ou de questions, veuillez nous contacter à l'adresse suivante: info-biathlon@froidmont.org
    """.trimIndent()

fun main(args: Array<String>) {
    RegistrationsApp()
}
