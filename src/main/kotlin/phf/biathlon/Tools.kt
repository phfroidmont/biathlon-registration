package phf.biathlon

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import java.io.File
import java.util.ArrayList

fun main(args: Array<String>) {
    val registrationsFile = File("/home/froidmpa/Downloads/registrations.json")
    val gson = GsonBuilder().setPrettyPrinting().create()
    val registrationsString = registrationsFile.readText()
    val registrations: List<Registration> = if (registrationsString.isEmpty())
        ArrayList()
    else
        gson.fromJson<List<Registration>>(registrationsString, object : TypeToken<List<Registration>>() {}.type).toMutableList()

//    listOf("email", "lastName", "firstName", "licenceNumber", "birthYear", "category", "weapon", "ownWeapon", "preferredTime").joinToString(",").let { println(it) }
//    registrations.map { listOf(it.email,it.lastName,it.firstName, it.licenceNumber,it.birthYear,it.category,it.weapon,it.ownWeapon,it.preferredTime).joinToString(",") }.forEach { println(it) }
    registrations.map { it.email }.joinToString(",").let { println(it) }
}